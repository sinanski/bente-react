import SceneProvider from "./SceneProvider";
import {useScene as useSceneHook} from "./store";

export const useScene = useSceneHook;
export default SceneProvider;
