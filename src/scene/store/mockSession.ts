type Value = boolean | number | string[];

export const mockSession = {
  dsgvo_agreed: true,
  times_visited: 1,
  is_read: true,
  visited_pages: ['/home']
};

export const setSession = (key: string, value: Value) => ({
  ...mockSession,
  [key]: value
});
