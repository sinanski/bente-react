import { StartSceneProps } from "../types";
import { useComponentMount } from "../../assets/useComponentMount";
import { useScene } from "./index";

export const useSceneStart = (config: StartSceneProps) => {
  const { start } = useScene();
  useComponentMount( () => start( config ) )
};
