import {getImage} from "./getImage";

jest.mock('../../../../../constants/chatImages');
jest.mock('../../../../../constants/dialogImages');

describe('getImage', () => {
  it( 'should return image object for given name', function () {
    const images = getImage('other');
    const expected = {
      chat_face: 'mock other face',
      chat_background: 'mock other background',
      dialog: 'mock other dialog'
    };

    expect(images).toEqual(expected)
  } );

  it( 'should return default image if no valid name is provided', function () {
    const images = getImage('no valid key');
    const expected = {
      chat_face: 'mock Bente face',
      chat_background: 'mock Bente background',
      dialog: 'mock Bente dialog'
    };

    expect(images).toEqual(expected)
  } );
});
