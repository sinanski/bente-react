import { SceneProps } from "../../../../types";

export const getDelay = (scenes: SceneProps[], sceneId: string) => {
  const next = scenes
    .filter( ({ id }) => (
      id === sceneId
    ) )[0] || {};
  return next.delay || 0;
};
