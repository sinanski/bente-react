import { SceneInterface } from "../../../../types";

export const getFrameIndex = (
  scene: SceneInterface,
  target: string
) =>
  scene.frames
    .map( frame => frame.id )
    .indexOf( target );
