import { getDelay } from "./getDelay";
import {mockStore} from "./mockStore";

describe('getDelay()', () => {
  it( 'should return delay of next scene', function () {
    const delay = getDelay(mockStore.state.scenes,'mock id 2');

    expect(delay).toBe('mock delay 2')
  } );
});
