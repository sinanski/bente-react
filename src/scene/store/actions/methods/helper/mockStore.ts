import { State } from "../../../actions/types";

export const frames = [
  'mock frame 1',
  'mock frame 2',
];

export const mockScene1 = {id: 'mock id', frames, delay: 'mock delay 1'};
export const mockScene2 = {id: 'mock id 2', frames, delay: 'mock delay 2', origin: 'mock page 2'};
export const mockScene3 = {id: 'mock id 3', frames, delay: 'mock delay 3', origin: 'GLOBAL'};

export const mockStore = {
  state: {
    scenes: [mockScene1, mockScene2, mockScene3],
    played: [],
    current: '',
    show: true,
    frameIndex: 1,
    is_animated_in: false,
    is_animated_out: false,
    delay: 'mock default delay',
    page: 'mock page'
  },
  setState: jest.fn(),
  actions: {
    hide: jest.fn(),
    finish: jest.fn(),
    getCurrentScene: jest.fn(() => mockScene1),
  }
};

export const setStore = (
  key: keyof State,
  value: any,
  store = mockStore
): typeof mockStore => {
  return {
    ...store,
    state: {
      ...store.state,
      [key]: value
    }
  }
};

export const setState = (
  key: keyof State,
  value: any,
): typeof mockStore => {
  return {
    ...mockStore,
    state: {
      ...mockStore.state,
      [key]: value
    }
  }
};
