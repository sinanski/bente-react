import {filterValid} from "./filterValid";
import {SceneValidation} from "./SceneValidation/SceneValidation";


jest.mock('./SceneValidation/SceneValidation', () => {
  const mockSceneValidation = jest.fn(() => ({
    global: jest.fn(mockSceneValidation),
    page: jest.fn(mockSceneValidation),
    session: jest.fn(mockSceneValidation),
  }));
  return {
    SceneValidation: mockSceneValidation
  }
});

describe('SceneValidation', () => {
  it( 'should call SceneValidation', function () {
    filterValid('mock state', 'mock scenes', 'mock session');

    expect(SceneValidation).toHaveBeenCalledWith('mock state', 'mock scenes', 'mock session');
  } );
});
