import { SceneValidation } from "./SceneValidation";
import { mockScene1, mockStore, setState, mockScene2, mockScene3 } from "../../mockStore";
import { mockSession, setSession } from "../../../../../mockSession";

describe( 'new SceneValidation()', () => {

  beforeEach(jest.clearAllMocks);

  it( 'should setup properly', function () {
    // expect(validate.scenes).toEqual(mockStore.state.scenes)
  } );

  it( 'should remove globals on validate.global()', function () {
    const disableGlobal = setState( 'disableGlobal', true );
    const validate = new SceneValidation(
      disableGlobal.state,
      mockStore.state.scenes,
      mockSession
    );

    const expectedScenes = [ mockScene1, mockScene2 ];
    validate.global();

    expect( validate ).toEqual( {
      ...validate,
      scenes: expectedScenes
    } )
  } );

  it( 'should remove non matching pages on validate.page()', function () {
    const withPage = setState( 'page', 'mock page 2' );
    const validate = new SceneValidation(
      withPage.state,
      mockStore.state.scenes,
      mockSession
    );

    const expectedScenes = [ mockScene2, mockScene3 ]
    validate.page();

    expect( validate ).toEqual({
      ...validate,
      scenes: expectedScenes
    })
  } );

  it( 'should remove scenes that have already been played with validate.session()', function () {
    const session = setSession('filtered_scenes', [mockScene2.id]);
    const validate = new SceneValidation(
      mockStore.state,
      mockStore.state.scenes,
      session
    );

    const expectedScenes = [ mockScene1, mockScene3 ];
    validate.session();

    expect( validate ).toEqual({
      ...validate,
      scenes: expectedScenes
    })
  } );
} );
