import { getUnPlayedScenes } from "./getUnPlayedScenes";
import { State } from "../../../types";

export const getUnPlayed = (state: State) => {
  const { scenes, played } = state;
  return getUnPlayedScenes( scenes, played );
};
