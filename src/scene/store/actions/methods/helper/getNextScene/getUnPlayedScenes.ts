import { SceneList } from "../../../../../types";

export const getUnPlayedScenes = (scenes: SceneList, played: string[]): SceneList =>
  scenes.filter( ({ id }) => (
    played.indexOf( id ) === -1
  ) );
