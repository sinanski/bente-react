import { getNextScene } from "./index";
import { filterValid } from "./filterValid";
import { getNextInOrder } from "./getNextInOrder";
import { getUnPlayed } from "./getUnPlayed";
import { addUnique } from "../addUnique";
import { getRepeatingScenes } from "./getRepeatingScenes";

jest.mock( './filterValid' );
jest.mock( './getUnPlayed' );
jest.mock( './getNextInOrder' );
jest.mock( '../addUnique' );
jest.mock( './getRepeatingScenes' );

const mock_session = 'mock session';
const mock_sequence = {scenes: ['mock scenes']};
const mock_unplayed = 'mock unplayed';
const mock_uniques = 'mock uniques';

describe( 'getNextScene', () => {

  beforeEach( jest.clearAllMocks );

  it( 'should call filterUnplayed', function () {
    getUnPlayed.mockReturnValue( mock_unplayed );
    filterValid.mockReturnValue( [] );
    addUnique.mockReturnValue( mock_uniques );
    getNextScene( mock_sequence, mock_session )

    expect( filterValid ).toHaveBeenCalledWith( mock_sequence, mock_uniques, mock_session );
    expect( getNextInOrder ).not.toHaveBeenCalled();
  } );

  it( 'should call getNextInOrder', function () {
    filterValid.mockReturnValue( [ mock_unplayed ] );
    getNextScene(mock_sequence);

    expect( getNextInOrder ).toHaveBeenCalledWith( [ mock_unplayed ] );
  } );
} );
