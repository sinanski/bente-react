import {getUnPlayed} from "./getUnPlayed";
import {mockStore, mockScene1, mockScene2, setState} from "../mockStore";
import {getUnPlayedScenes} from "./getUnPlayedScenes";

jest.mock('./getUnPlayedScenes');

describe('getUnplayed() [scenes]', () => {

  it( 'should use either provided played list or state.played', function () {
    const playedState = setState('played', [mockScene1.id]);
    getUnPlayed(playedState.state);

    expect(getUnPlayedScenes).toBeCalledWith(playedState.state.scenes, playedState.state.played)
  } );
});
