import { SceneList } from "../../../../../types";

export const getRepeatingScenes = (scenes: SceneList) =>
  scenes.filter( ({ endless }) => endless );
