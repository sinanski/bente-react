import {getUnPlayedScenes} from "./getUnPlayedScenes";
import { mockStore, mockScene1, mockScene2, mockScene3 } from "../mockStore";

describe('getUnPlayedScenes', () => {
  it( 'should return an array of all unplayed scenes', function () {
    const playAll = getUnPlayedScenes(mockStore.state.scenes, []);

    expect(playAll).toEqual(mockStore.state.scenes)
  } );

  it( 'should skip first mock if it is already played', function () {
    const playTwo = getUnPlayedScenes(mockStore.state.scenes, [mockScene1.id]);
    const playOne = getUnPlayedScenes(mockStore.state.scenes, [mockScene1.id, mockScene3.id]);
    const empty = getUnPlayedScenes(mockStore.state.scenes, [mockScene1.id, mockScene2.id, mockScene3.id]);

    expect(playTwo).toEqual([mockScene2, mockScene3]);
    expect(playOne).toEqual([mockScene2]);
    expect(empty).toEqual([]);
  } );
});
