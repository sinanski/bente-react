import { addScene } from "./addScene";
import { mockStore, mockScene1, mockScene2 as second } from "./helper/mockStore";
import { createScene } from "../../../constructor/createScene";

jest.mock( '../../../constructor/createScene' );

// todo here missing tests for the updates when added scene is instantly played

const mockScene = {
  ...mockScene1,
  id: 'mock new id'
};

const mockScene2 = {
  ...second,
  id: 'mock new id 2'
};

describe( 'scene.addScene()', () => {

  it( 'should add the provided scene', function () {
    createScene.mockReturnValue( mockScene );
    const newScenes = addScene( mockStore.state.scenes, mockScene );

    beforeEach(() => {
      jest.clearAllMocks()
    });

    const expected =  mockStore.state.scenes.concat( mockScene );

    expect( createScene ).toHaveBeenCalledWith( mockScene );
    expect( newScenes ).toEqual( expected );
  } );

  it( 'should add scene with origin', function () {
    createScene.mockReturnValue( mockScene );
    const newScenes = addScene( mockStore.state.scenes, mockScene, 'mock origin' );

    const expectedOrigin = {
      ...mockScene,
      origin: 'mock origin'
    };

    const expected = mockStore.state.scenes.concat( expectedOrigin );

    expect( newScenes ).toEqual( expected );
  } );

  it( 'should add an array of scenes', function () {
    createScene
    .mockReturnValueOnce(mockScene)
    .mockReturnValueOnce(mockScene2);
    const newScenes =  addScene( mockStore.state.scenes, [mockScene, mockScene2] );

    const expected = mockStore.state.scenes.concat( mockScene, mockScene2 );

    expect( createScene ).toHaveBeenCalledWith( mockScene );
    expect( createScene ).toHaveBeenCalledWith( mockScene2 );
    expect( newScenes ).toEqual( expected );
  } );
} );
