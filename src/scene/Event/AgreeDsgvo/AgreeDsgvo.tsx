import { useSession } from "../../../session/store";
import { useComponentMount } from "../../../assets/useComponentMount";


const AgreeDsgvo = () => {
  const {agreeToDsgvo} = useSession();
  useComponentMount(agreeToDsgvo);

  return null
}

export default AgreeDsgvo
