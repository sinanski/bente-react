import {createChoices} from "./createChoices";

const given = [
  'mock choice 1',
  'mock choice 2',
  {
    text: 'choice 3',
    target: 'mock target'
  }
];

const expected = [
  { text: 'mock choice 1' },
  { text: 'mock choice 2' },
  {
    text: 'choice 3',
    target: 'mock target'
  }
  ];

describe('scene constructor createChoices()', () => {
  it( 'should create an object from string', function () {
    const choices = createChoices(given);
    const choices2 = createChoices(expected);

    expect(choices).toEqual(expected);
    expect(choices2).toEqual(expected);
  } );
});
