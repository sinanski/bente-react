import {getText} from "./getText";

const mockFrames = [
  {text: 'mock 1'},
  {text: ''},
  {text: 'mock 2'},
];

const mockFrames2 = [
  {text: 'mock 1'},
  {},
  {text: 'mock 2'},
]

describe('getText', () => {
  it( 'should return the current text if available', function () {
    const text = getText(mockFrames, 0);
    const text2 = getText(mockFrames, 2);

    expect(text).toBe('mock 1')
    expect(text2).toBe('mock 2')
  } );

  it( 'should return the last text if no current is available', function () {
    const text = getText(mockFrames, 1);

    expect(text).toBe('mock 1')
  } );

  it( 'should return the last text if current is undefined', function () {
    const text = getText(mockFrames2, 1);

    expect(text).toBe('mock 1')
  } );

});
