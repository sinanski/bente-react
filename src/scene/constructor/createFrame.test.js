import {createFrame} from "./createFrame";
import input from './test_files/screens/input.json';
import output from './test_files/screens/output.json';

const mockSceneDefault = {
  image: 'mock default image',
  name: 'mock default name',
};


describe('createScene()', () => {
  it( 'should create default scene', function () {
    const scene = createFrame( [ input ], mockSceneDefault );

    expect(scene[0]).toMatchObject({
      ...output,
      id: expect.any(String),
      ...mockSceneDefault
    });
  } );

  it( 'should pass id', function () {
    const props = {
      id: 'mock id',
      type: 'ANSWER',
      target: 'mock target',
      image: 'mock image',
    };
    const scene = createFrame( [ {
      ...input,
      ...props
    } ],
      mockSceneDefault
    );

    expect(scene[0]).toMatchObject({
      ...output,
      ...props
    });
  } );
});
