import { createScene } from "./createScene";
import input from './test_files/dialog/input.json';
import output from './test_files/dialog/output.json';

describe( 'createScene()', () => {
  it( 'should ', function () {
    const interaction = createScene(input);

    expect(interaction).toMatchObject(output)
  } );
} );
