import { createScene} from "./createScene";
import { createFrame } from "./createFrame";
import { id } from "../../utilz/id";

jest.mock( './createFrame' );
jest.mock( '../../utilz/id' );

const mockFrames = [{text: 'mock frame'}];

const expected = {
  id: 'mock id',
  type: 'DIALOG',
  image: 'bente',
  name: 'Bente',
  endless: false,
  frames: mockFrames,
  delay: 1,
  order: 100,
  origin: 'LOCAL',
  repeat: false,
};

describe( 'createScene()', () => {

  beforeEach(() => {
    id.mockReturnValue('mock id');
    createFrame.mockReturnValue(mockFrames);
    jest.clearAllMocks();
  });

  it( 'should create Scene from string', function () {
    const scene = createScene('mock text');

    expect(scene).toEqual(expected);
  } );

  it( 'should create Scene from partial scene', function () {
    const partial = {
      frames: mockFrames,
      id: 'special id',
      order: 'mock order'
    };
    const scene1 = createScene(partial);

    expect(scene1).toEqual({...expected, ...partial});
  } );

  it( 'should create Scene from scene', function () {
    const given = {
      id: 'mock id special',
      type: 'DIALOG special',
      image: 'bente special',
      name: 'name special',
      endless: 'mock endless',
      frames: mockFrames,
      delay: 'mock delay special',
      order: 'mock order special',
      origin: 'LOCAL specialx',
      repeat: 'mock repeat'
    };
    const scene = createScene(given);

    expect(scene).toEqual(given);
  } );
} );
