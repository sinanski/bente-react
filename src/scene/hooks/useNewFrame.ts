import { Frame } from "../types";
import { useCallback } from "react";
import { usePrevious } from "../../assets/usePrevious";
import { useScene } from "../index";

const hasValue = (object?: { [index: string]: any }) => {
  if ( !object ) {
    return false
  }
  return Object.values( object ).length;
};

export const useNewFrame = (frame: Frame) => {
  const {getCurrentScene} = useScene();
  const lastFrame = usePrevious( frame );

  return useCallback(
    () =>
      hasValue( lastFrame )
      && hasValue( frame )
      && frame !== lastFrame
      && getCurrentScene().type === 'CHAT',
    [frame, getCurrentScene, lastFrame]
  );
};
