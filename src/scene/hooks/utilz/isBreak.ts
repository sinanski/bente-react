import { Frame } from "../../types";

export const isBreak = (
  nextFrame: Frame,
  target?: string,
) =>
  (!nextFrame.type && !target)
  || target === 'break';
