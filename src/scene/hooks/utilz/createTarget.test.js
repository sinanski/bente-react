import { createTarget } from "./createTarget";
import { createNewTarget } from "./createNewTarget";
import { isBreak } from "./isBreak";

jest.mock( './createNewTarget' );
jest.mock( './isBreak' );

const expectBreak = {
  isValid: false,
  id: '',
  break: 'mock break'
};

describe( 'helper > createTarget()', () => {

  beforeEach( jest.clearAllMocks );

  it( 'should call createNewTarget and isBreak', function () {
    createNewTarget.mockReturnValue('mock new target');
    isBreak.mockReturnValue('mock break');
    createTarget( 'current frame mock', 'next frame mock', 'target mock' );

    expect(createNewTarget).toHaveBeenCalledWith('current frame mock', 'target mock');
    expect(isBreak).toHaveBeenCalledWith("next frame mock", "mock new target");
  } );

  it( 'should return the right object', function () {
    const a = createTarget();
    const expected = {
      isValid: true,
      id: 'mock new target',
      break: 'mock break'
    };

    expect(a).toEqual(expected);
  } );

  it( 'should validate false if current target is not given', function () {
    createNewTarget.mockReturnValue(undefined);
    const a = createTarget();

    expect(a).toEqual(expectBreak);
  } );

  it( 'should validate false if current target is empty', function () {
    createNewTarget.mockReturnValue('');
    const a = createTarget();
    const expected = {
      has: false,
      target: '',
      break: 'mock break'
    };

    expect(a).toEqual(expectBreak);
  } );
} );
