import { Frame } from "../../types";

export const createNewTarget = (
  currentFrame: Frame,
  target?: string,
) => target && target.length
  ? target
  : currentFrame.target;
