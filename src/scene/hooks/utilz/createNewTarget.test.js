import {createNewTarget} from "./createNewTarget";

const mockFrame = {target: 'mock target'};

describe('helper > createNewTarget()', () => {
  it( 'should create a target from either the provided one or taken from current frame', function () {
    const target_empty = createNewTarget({});
    const target_provided = createNewTarget({}, 'mock from given');
    const both_provided = createNewTarget(mockFrame, 'mock from given');
    const frame_provided = createNewTarget(mockFrame);

    expect(target_empty).not.toBeDefined();
    expect(target_provided).toBe('mock from given');
    expect(both_provided).toBe('mock from given');
    expect(frame_provided).toBe(mockFrame.target);
  } );
});
