import {isBreak} from "./isBreak";

const type = {type: 'mock type'};

describe( 'helper > isBreak()', () => {
  it( 'it should check for scene break', () => {
    const no_next_frame = isBreak({});
    const target_break = isBreak(type, 'break');
    const no_break = isBreak(type, 'some else');
    const no_break_because_of_target = isBreak({}, 'some else');

    expect(no_next_frame).toBe(true);
    expect(target_break).toBe(true);
    expect(no_break).toBe(false);
    expect(no_break_because_of_target).toBe(false);
  } )
} );
