import { Hooks } from "../about/Hooks";

export const createPlaceholder = (text: string) => ({
  text,
  target: Hooks.PLACEHOLDER
});
