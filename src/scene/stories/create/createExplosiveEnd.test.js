import {createExplosiveEnd} from "./createExplosiveEnd";
import { SceneEvent } from "../../types";

describe( 'createExplosiveEnd', () => {
  it( 'should return an ending frame with an explosion', () => {
    const explosion = createExplosiveEnd('mock text');
    const expected = {
      text: 'mock text',
      target: 'break',
      event: SceneEvent.EXPLOSION
    };

  expect(explosion).toEqual(expected);
  } )
} );
