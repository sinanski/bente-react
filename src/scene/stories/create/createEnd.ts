import { FrameProps } from "../../types";

export const createEnd = (text: string = 'Bente hat den Chat beendet'): FrameProps => ({
  text,
  target: 'break'
});
