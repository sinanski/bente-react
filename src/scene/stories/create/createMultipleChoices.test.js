import {createMultipleChoices} from "./createMultipleChoices";

describe('createChoices', () => {
  it( 'should create choices from array', function () {
    const created = createMultipleChoices(
      ['CHAT_BOT', "ChatBot activate?"],
      ['HUMAN', "Hallo?"]
    );

    const expectedChoices = [
      {
        "target": 'CHAT_BOT',
        "text": "ChatBot activate?"
      },
      {
        "target": 'HUMAN',
        "text": "Hallo?"
      }
    ];

    const expected = {
      "type": "ANSWER",
      choices: expectedChoices
    };

  expect(created).toEqual(expected)
  } );
});
