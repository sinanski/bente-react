import {createDefaultImage} from "./createDefaultImage";
import {next} from "./next";

jest.mock('./next');


describe( 'createDefaultImage', () => {

  it( 'should call next to create a real scene', () => {
    next.mockReturnValue([]);
    createDefaultImage('mock image', 'mock file');

    expect(next).toHaveBeenCalledWith('mock image', 'mock file')
  } );
  it( 'should update all frames with the default image', () => {
    next.mockReturnValue([
      {text: 'foo'},
      {text: 'bar'},
    ]);
    const added_image = createDefaultImage('mock image');

    const expected = [
      {text: 'foo', image: 'mock image'},
      {text: 'bar', image: 'mock image'},
    ];

    expect(added_image).toEqual(expected)
  } );

  it( 'should not update if the current frame has an image', () => {
    next.mockReturnValue([
      {text: 'foo'},
      {text: 'bar', image: 'mock own image'},
    ]);
    const added_image = createDefaultImage('mock image');
    const expected = [
      {text: 'foo', image: 'mock image'},
      {text: 'bar', image: 'mock own image'},
    ];

    expect(added_image).toEqual(expected)
  } )
} );
