import {createChoicesIrrelevant} from "./createChoicesIrrelevant";

describe( 'createChoicesIrrelevant', () => {
  it( 'should create a list of choices all with the same target', () => {
    const target = 'mock target'
    const choices = createChoicesIrrelevant(target, ['mock 1', 'mock 2']);
    const expected = {
      "type": "ANSWER",
      "choices": [
        {
          text: 'mock 1',
          target
        },
        {
          text: 'mock 2',
          target
        }
      ]
    };

    expect(choices).toEqual(expected);
  } )
} );
