import { SceneEvent } from "../../types";

export const createExplosiveEnd = (text: string) => ({
  text,
  target: 'break',
  event: SceneEvent.EXPLOSION
});
