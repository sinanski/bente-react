import {next} from "./next";

const input = [
  'foo',
  'bar',
  {mock_key: 'mock_value', text: 'mock_text'}
];

const input2 = [
  'mock',
  {text: 'me', id: 'mock second id'},
  {choices: 'mock choices'}
];

const expected_undefined = {
  id: undefined,
  target: undefined
};

const expected = [
  {text: 'foo', id: 'mock id'},
  {...expected_undefined, text: 'bar'},
  {...expected_undefined, mock_key: 'mock_value', text: 'mock_text'}
];

const expected2 = [
  { ...expected_undefined, text: 'mock', id: 'mock id 2'},
  {...expected_undefined, text: 'me', id: 'mock second id'},
  {...expected_undefined, choices: 'mock choices'}
];

describe( 'next', () => {
  it( 'should provide valid Frameprops', () => {

    const next_part = next('mock id', input);
    const next_part2 = next('mock id 2', input2);

    expect(next_part).toEqual(expected);
    expect(next_part2).toEqual(expected2);
  } );

  it( 'should add break to last element if break_scene is true', function () {
    const next_part = next('mock id', input, true);

    let expect_break = expected.slice();
    expect_break[2] = {
      ...expect_break[2],
      target: 'break'
    };

    expect(next_part).toEqual(expect_break)
  } );
} );
