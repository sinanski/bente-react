import { Pages } from "../../../constants/Pages";

export const backToBente = (text: string) => ({
  text,
  target: Pages.ABOUT
})