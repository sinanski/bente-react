import {createSingleChoice} from "./createSingleChoice";

describe( 'createSingleChoice', () => {
  it( '', () => {

    const choice = createSingleChoice('mock choice');

    const expectedChoices = [{
      "target": undefined,
      "text": "mock choice"
    }];

    const expected = {
      "type": "ANSWER",
      choices: expectedChoices
    };

    expect(choice).toEqual(expected);

  } )
} );
