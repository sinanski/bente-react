import {createChoices} from "./createChoices";

describe( 'createChoices', () => {
  it( 'should create choices with target attached to first', () => {
    const choices = createChoices(
      'mock id'
      , ['mock choice 1', 'mock choice 2']
    );
    const expectedChoices = [
      {
        "target": 'mock id',
        "text": "mock choice 1"
      },
      {
        "target": undefined,
        "text": "mock choice 2"
      }
    ];

    const expected = {
      "type": "ANSWER",
      choices: expectedChoices
    };

    expect(choices).toEqual(expected)
  } );

  it( 'should create choices with target attached to first', () => {
    const choices = createChoices(
      'mock id',
      ['mock choice 1', 'mock choice 2'],
      true
    );
    const expectedChoices = [
      {
        "target": undefined,
        "text": "mock choice 1"
      },
      {
        "target": 'mock id',
        "text": "mock choice 2"
      }
    ];

    const expected = {
      "type": "ANSWER",
      choices: expectedChoices
    };

    expect(choices).toEqual(expected)
  } )
} );
