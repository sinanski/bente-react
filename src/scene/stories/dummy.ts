import { SceneProps } from "../types";
import { DialogImage } from "../../constants/dialogImages";

export const dummy: SceneProps = {
  id: 'greeting',
  image: DialogImage.BENTE,
  // delay: 4,
  order: 10,
  // type: 'CHAT',
  frames: [
    {
      "text": "He na, du hier.",
    },
    {
      "image": DialogImage.BENTE_SMIRK,
      "text": "Was suchst du hier?"
    },

    {
      "id": "good",
      "image": DialogImage.BENTE_HAPPY,
      "type": "TEXT",
      "text": "DANN bist du hier genau richtig! Klick mal oben auf das Burger-Menü ;o)"
    },
    {
      "id": "bad",
      "text": "CHATBOT ACTIVATE"
    },
    {
      "text": "Wenn du zu den Nacktaktiven Fledermäusen willst, sag bitte laut  NACKTAKTIVE FLEDERMÄUSE"
    },
    {
      "text": "Wenn du mein Portfolio sehen willst, dann drücke drei mal Shift."
    },
    {
      "text": "Wenn du mir eine Nachricht schreiben willst, dann geh zu Googlemail, wie jeder vernünftige Boomer."
    },

    {
      "text": "Ok, rufe \"Mama Festnetz\" an. Ist das richtig?"
    },

    {
      "text": "Ich bin kein Chatbot! ICH BIN DER BENTE BOT! Und jetzt drücke oben auf das Burgermenü und find dich selbst zurecht."
    }
  ]
};

export default dummy;