import { useCallback, useEffect, useState } from "react";
import { useScene } from "../../index";
import { usePrevious } from "../../../assets/usePrevious";

export const useChoices = () => {
  const scene = useScene();
  const { choices = [] } = scene.getCurrentFrame();
  const prevChoices = usePrevious( choices ) || [];

  const [ currentChoices, setCurrentChoices ] = useState( choices );
  const [ choicesReady, setChoicesReady ] = useState( false );

  const hasChoices = useCallback(
    () => choices && choices.length,
    [ choices ]
  );

  const needsReset = useCallback(
    () => !choices.length && prevChoices.length,
    [ choices.length, prevChoices.length ]
  );

  const resetChoices = () => {
    setCurrentChoices( [] );
    setChoicesReady( false )
  };

  useEffect( () => {
    if ( hasChoices() ) {
      setCurrentChoices( choices )
    }
  }, [ choices, hasChoices ] );

  useEffect( () => {
    if ( needsReset() ) {
      resetChoices()
    }
  }, [ needsReset ] );

  return {
    choices: currentChoices,
    setReady: () => setChoicesReady( true ),
    ready: choicesReady
  }
};
