export interface RefProps {
  mobileRef: HTMLElement;
  screenRef: HTMLElement;
  screenBgRef: HTMLElement;
  loadingRef: HTMLElement;
  pathRef: HTMLElement;
  messageRef: HTMLElement;
}
