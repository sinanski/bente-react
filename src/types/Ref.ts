export type Ref = {current: HTMLElement | null};
export type SafeRef = {current: HTMLElement};