export type State = {
  isOpen: boolean;
  ready: boolean;
  isDisabled: boolean;
}

export type Actions = {
  open(): void;
  close(): void;
  toggle(): void;
  setReady(): void;
  disable(): void;
  enable(): void;
}
