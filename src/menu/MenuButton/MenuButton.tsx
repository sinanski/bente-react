import React from 'react';
import { Button } from "./Style";
import { useMenu } from "../store";
import Menu from "../index";

const MenuButton = () => {
  const [ { isOpen, isDisabled }, { toggle } ] = useMenu();

  return (
    <>
      <Button
        open={ isOpen }
        onClick={ toggle }
        disabled={isDisabled}
      />
      <Menu />
    </>
  );
};

export default MenuButton
