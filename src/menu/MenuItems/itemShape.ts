import { ShapeConstructor } from "../../shapes";

export const itemShape = () => new ShapeConstructor()
    .rectangle({fill: '#56e4ff78'})
    .randomize( { x: 10, y: 2 } )
    .add()
    .grow( { fill: 'white', x: 3, y: -1 } )
    .randomize( { extend: false, x: 20 } )
    .add();
