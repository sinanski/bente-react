import styled from "styled-components";

export const Hideable = styled.div<{hide: boolean}>`
  display: ${props => props.hide ? 'none' : 'flex'};
`;
