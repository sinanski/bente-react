export type LetterType = {
  letter: string;
  rotate: boolean;
  displace: boolean;
  invert: boolean;
}

export type Config = {
  rotate: boolean;
  displace: boolean;
  invert: boolean;
}
