import { calcInverts } from "./calcInverts";

describe( 'calcInverts', () => {

  it( 'should always return invert for first word', () => {
    const first = calcInverts( 'I' );

    const expected = [
      {
        word: 'I',
        possible_inverts: 1,
        certain_inverts: 1
      },
    ];

    expect( first ).toEqual( expected );
  } );

  it( 'should return an array of objects with settings for letter invert', () => {
    const first = calcInverts( 'I am a test' );

    const expected = [
      {
        word: 'I',
        possible_inverts: 1,
        certain_inverts: 1
      },
      {
        word: 'am',
        possible_inverts: 0,
        certain_inverts: 0,
      },
      {
        word: 'a',
        possible_inverts: 0,
        certain_inverts: 0,
      },
      {
        word: 'test',
        possible_inverts: 1,
        certain_inverts: 0,
      },
    ];

    expect( first ).toEqual( expected );
  } );

  it( 'should return many possible_inverts if word is longer than 5 letters', () => {
    const long = calcInverts( 'Interesting' );

    const expected = [
      {
        word: 'Interesting',
        possible_inverts: 3,
        certain_inverts: 1,
      },
    ];

    expect( long ).toEqual( expected );
  } )
} );
