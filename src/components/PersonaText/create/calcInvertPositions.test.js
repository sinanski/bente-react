import { calcInvertPositions } from "./calcInvertPositions";

describe( 'calcInvertPositions', () => {

  afterEach(() => {
    jest.spyOn(global.Math, 'random').mockRestore();
  });

  it( 'should return true if random condition is met', () => {
    jest.spyOn(global.Math, 'random')
    .mockReturnValue(1)
    .mockReturnValueOnce(.2)
    .mockReturnValueOnce(.2)
    .mockReturnValueOnce(.1);
    const result = calcInvertPositions('Test', 2);

    const expected = [
      { letter: 'T', invert: true, rotate: true, displace: true },
      { letter: 'e', invert: false, rotate: false, displace: false },
      { letter: 's', invert: false, rotate: false, displace: false },
      { letter: 't', invert: false, rotate: false, displace: false }
    ];

    expect(result).toEqual(expected);
  } );

  it( 'should never return two consecutive inverts', () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(.1);
    const result = calcInvertPositions('Testing', 2);

    const expected = [
      { letter: 'T', invert: true, rotate: true, displace: true },
      { letter: 'e', invert: false, rotate: true, displace: true },
      { letter: 's', invert: true, rotate: true, displace: true },
      { letter: 't', invert: false, rotate: true, displace: true },
      { letter: 'i', invert: false, rotate: true, displace: true },
      { letter: 'n', invert: false, rotate: true, displace: true },
      { letter: 'g', invert: false, rotate: true, displace: true }
    ];

    expect(result).toEqual(expected);
  } );

  it( 'never return more inverts than allowed', () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(.1);
    const result = calcInvertPositions('Test', 1);

    const expected = [
      { letter: 'T', invert: true, rotate: true, displace: true },
      { letter: 'e', invert: false, rotate: true, displace: true },
      { letter: 's', invert: false, rotate: true, displace: true },
      { letter: 't', invert: false, rotate: true, displace: true },
    ];

    expect(result).toEqual(expected);
  } )
} );
