export const randomFromLength = (
  arr: any[]
) =>
  Math.round( Math.random() * (arr.length - 1) );
