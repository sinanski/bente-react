import { calcCertainInverts } from "./calcCertainInverts";
import { randomFromLength } from "./randomFromLength";

jest.mock('./randomFromLength');

const raw = [
  { letter: 'T', invert: false, rotate: true, displace: true },
  { letter: 'e', invert: false, rotate: false, displace: false },
  { letter: 's', invert: false, rotate: false, displace: false },
  { letter: 't', invert: false, rotate: false, displace: false }
];

const expected = [
  { letter: 'T', invert: true, rotate: true, displace: true },
  { letter: 'e', invert: false, rotate: false, displace: false },
  { letter: 's', invert: true, rotate: false, displace: false },
  { letter: 't', invert: false, rotate: false, displace: false }
];

describe( 'calcCertainInverts', () => {
  it( 'should return original if number of inverts >= certain_inverts', () => {
    const update = calcCertainInverts( expected, 2 );

    expect( update ).toEqual( expected );
  } );

  it( 'should return original if too many certain_inverts are expected', () => {
    const update = calcCertainInverts( expected, 3 );

    expect( update ).toEqual( expected );
  } );

  it( '', () => {
    randomFromLength
    .mockReturnValueOnce( 2 )
    .mockReturnValueOnce( 0 );
    const update = calcCertainInverts( raw, 2 );

    const expected = [
      { letter: 'T', invert: true, rotate: true, displace: true },
      { letter: 'e', invert: false, rotate: false, displace: false },
      { letter: 's', invert: true, rotate: false, displace: false },
      { letter: 't', invert: false, rotate: false, displace: false }
    ];

    expect( update ).toEqual( expected );
  } )
} );
