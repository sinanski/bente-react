import { Categories, State } from "./types";

export const initialState = {
  isLoading: false,
  categories: [] as Categories,
  openCategory: '',
  openImage: -1,
  isSliderOpen: false,
};

type Action = {
  type: string;
  [index: string]: any;
}

export default function reducer(state: State = initialState, action: Action): State {
  switch ( action.type ) {

    case 'FETCH_IMAGES':
      return {
        ...state,
        isLoading: true,
      }

    case 'STORE_IMAGES':
      return {
        ...state,
        isLoading: false,
        categories: action.images as Categories
      }

    case 'SET_CATEGORY':
      return {
        ...state,
        openCategory: action.name
      }

    case 'RESET_CATEGORY':
      return {
        ...state,
        openCategory: initialState.openCategory
      }

    case 'SET_IMAGE':
      return {
        ...state,
        openImage: action.image as number
      }

    case 'OPEN_SLIDER':
      return {
        ...state,
        isSliderOpen: true,
      }

    case 'CLOSE_SLIDER':
      return {
        ...state,
        isSliderOpen: false,
      }

    default: return state
  }
};
