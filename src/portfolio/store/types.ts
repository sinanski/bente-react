import { initialState } from "./reducers";

export type State = typeof initialState

export type Categories = Category[];

export type Category = {
  icon: string;
  iconOmo: string;
  images: Image[];
  name: string;
}

export type Image = {
  filename: string;
  title: string;
  categories?: string[];
  targetSize?: number;
}