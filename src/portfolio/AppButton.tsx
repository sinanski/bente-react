import React, { useState } from 'react';
import { Container, Image, Name } from "./AppButtonStyles";
import { CategoryType } from "./useApps";

interface Props extends CategoryType {
  onClick(): void;
}

const AppButton: React.FC<Props> = (
  {
    name,
    img,
    img_animated,
    onClick
  }
) => {
  const [src, setImage] = useState(img);

  const onMouseOver = () => {
    setImage(img_animated)
  };

  const onMouseOut = () => {
    setImage(img)
  };


  return (
    <Container onClick={onClick}>
      <Image
        onMouseOver={onMouseOver}
        onMouseOut={onMouseOut}
        src={src}
      />
      <Name>{name}</Name>
    </Container>
  );
};

export default AppButton
