import { State as SequenceState } from "../../scene/store/actions/types";
import { State as ChatState } from "../../scene/Chat/store/actions/types";
import { State as TestState } from "../../TestRig/store/reducers";
import { State as SessionState } from "../../session/store/types";
import { State as ImagesState } from "../../portfolio/store/types";

export type Store = {
  sequence: SequenceState;
  chat: ChatState;
  test: TestState;
  session: SessionState;
  images: ImagesState;
}
