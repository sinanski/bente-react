// todo This is a stupid wrapper only needed because my setup won't work
//  Needs cleaning when I got time.

import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, render, configure } from 'enzyme';

configure({ adapter: new Adapter() });

export {
  shallow,
  mount,
  render,
  configure
};
