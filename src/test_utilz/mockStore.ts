export const mockSession = {
  times_visited: 1,
  played: ['mock played scene'],
  filtered_scenes: ['mock filtered scene'],
  visited_pages: ['/mock_page']
};

export const setMockSession = (
  key: keyof typeof mockSession,
  value: number | string[]
) => ({
  ...mockSession,
  [key]: value
});


export const frames = [
  'mock frame 1',
  'mock frame 2',
];

export const mockScene1 = {id: 'mock id', frames, delay: 'mock delay 1'};
export const mockScene2 = {id: 'mock id 2', frames, delay: 'mock delay 2', origin: 'mock page 2'};
export const mockScene3 = {id: 'mock id 3', frames, delay: 'mock delay 3', origin: 'GLOBAL'};


const mockSequence = {
  scenes: [mockScene1, mockScene2, mockScene3],
  played: [],
  current: '',
  show: true,
  frameIndex: 1,
  is_animated_in: false,
  is_animated_out: false,
  delay: 'mock default delay',
  page: 'mock page'
};

export const setMockSequence = (
  key: keyof typeof mockSequence,
  value: any
) => ({
  ...mockSequence,
  [key]: value
});

export const mockState = {
  session: mockSession,
  sequence: mockSequence,
};

export const mockStore = {
  useStore: jest.fn(() => ({
    getState: jest.fn(() => mockState)
  }))
};
