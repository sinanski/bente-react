import { renderHook } from '@testing-library/react-hooks';
import { useSessionForValidation } from "./useSessionForValidation";
import { useSelector } from "react-redux";
import {mockState} from "../../test_utilz/mockStore";

jest.mock( "react-redux", () => ({
  ...jest.requireActual( "react-redux" ),
  useSelector: jest.fn()
}) );

describe( 'useSessionForValidation', () => {

  beforeEach( () => {
    useSelector.mockImplementation( callback =>
       callback( mockState )
     );
  } );

  afterEach(useSelector.mockClear);

  it( 'should return all elements that are passed to validation callback', () => {
    const { result } = renderHook( () => useSessionForValidation() );

    const expected = {
      filtered_scenes: mockState.session.filtered_scenes,
      times_visited: mockState.session.times_visited,
      visited_pages: mockState.session.visited_pages,
      played: mockState.sequence.played
    };

    expect(result.current).toEqual(expected);
  } )
} );
