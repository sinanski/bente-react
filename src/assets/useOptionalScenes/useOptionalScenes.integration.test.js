import { renderHook } from '@testing-library/react-hooks';
import { useOptionalScenes } from "./useOptionalScenes";
import { useSessionForValidation } from "./useSessionForValidation";
import Dialog_Text from "../../constants/Dialog_Text";
import { DialogImage } from "../../constants/dialogImages";
import { greeting } from "../../scene/stories/greeting";
import {mockSession, setMockSession} from "../../test_utilz/mockStore";

jest.mock( './useSessionForValidation' );

const scene2 = {
  id: 'visit 2',
  frames: [ { text: Dialog_Text.VISIT_TIMES_2 } ]
};

const scene5 = {
  id: 'visit 5',
  image: DialogImage.BENTE_SMIRK,
  frames: [ { text: Dialog_Text.VISIT_TIMES_5 } ]
};

const optionalScenes = [
  greeting,
  {
    scene: scene2,
    times_visited: 2
  },
  {
    scene: scene5,
    times_visited: 5
  }
 ];

describe( 'useOptionalScenes integration test', () => {

  it( 'should return only first valid scene in array of scenes', () => {
    const session = setMockSession("times_visited", 5)
    useSessionForValidation.mockReturnValue(session)
    const { result } = renderHook( () => useOptionalScenes( [optionalScenes] ) );

    expect(result.current[0].id).toBe('greeting');
    expect(result.current).toHaveLength(1);
  } );

  it( 'should return second scene in array if first is not valid (played in this case)', () => {
    const session = {
      ...mockSession,
      filtered_scenes: ['greeting'],
      times_visited: 3
    };

    useSessionForValidation.mockReturnValue(session)
    const { result } = renderHook( () => useOptionalScenes( [optionalScenes] ) );

    expect(result.current[0].id).toBe('visit 2');
    expect(result.current).toHaveLength(1);

  } );

  it( 'should return all valid scenes if they are not in a separate validation array', () => {
    const session = {
      ...mockSession,
      times_visited: 7
    };

    useSessionForValidation.mockReturnValue(session);
    const { result } = renderHook( () => useOptionalScenes( optionalScenes ) );
    const ids = result.current.map(({id}) => id);

    expect(ids).toEqual(['greeting', 'visit 2', 'visit 5'])
  } )
} );
