import { SceneProps } from "../../scene/types";

export type OptionalScene = {
  scene: SceneProps;
  times_visited?: number;
  validate?(session: SessionState): boolean;
}
export type PossibleSceneType = OptionalScene | SceneProps;
export type OptionalScenes = PossibleSceneType[] | PossibleSceneType[][];

export type SessionState = {
  filtered_scenes: string[];
  visited_pages: string[];
  played: string[];
  times_visited: number
}

export type ValidatableScene = {
  scene: SceneProps;
  times_visited: number;
  validate(session: SessionState): boolean;
}