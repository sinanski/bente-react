#useOptionalScenes

useOptionalScenes() is beeing used to generate a list of possible scenes that can be played on a page. It receives an array of possible scenes together with their validators.

    useOptionalScenes([
      {
        scene,
        validate: (session) => !!session.played.length
      },
    ])
    
###validators
Each validator gets the whole session with the added array of the played scenes as param. The validation happens as soon as scene is ready and only <ins>once on initial ready</ins>.

##Built in validation
Sometimes scenes should be played depending on the times a user has visited the page.
<br/>
There is a built in way to easily achieve this.

###Simple Check

    useOptionalScenes([
        {
            scene,
            times_visited: 3
        }
    ])
    
This will just check, if the user has visited the website (not this specific page) at least 3 times.

###List checks
Sometimes different scenes should be played depending on the amount of time the user has visited to page.

    useOptionalScenes([
        [
            {
                scene: scene1,
            },
            {
                scene: scene2,
                times_visited: 3
            },
            {
                scene: scene3,
                times_visited: 5
            },
        ]
    ])

By providing an array with scenes (within the array) those scenes will be treated specially. Only one of the provided scenes will be added to the playlist.
<br/><br/>
It does not only check if the amount of visits is sufficient, but will also play the provided scenes in line. So in the above example even if a visitor has visited the page 5 times, but `scene2` has never been played it will play `scene2`.
####Short hand version
You can also use

    useOptionalScenes([[
        scene1,
        scene2,
    ]])
This will check if scene1 is already played. If so it will add scene2 otherwise it will ass scene1.

###Combine methods
All methods can be combined. In the below example this means.
- Validate scene1. If at least one scene is in played, add scene2 to playlist
- validate if scene2 has been played. If not add it to the playlist. If so add scene3 to playlist.
- scene4 will only be played if scene2 and scene3 have been played AND scene4's validation returns true.


    useOptionalScenes([
      {
        scene: scene1,
        validate: (session) => !!session.played.length
      },
      [
        scene2,
        scene3,
        {
          scene: scene4,
          validate: (session) => session.screen_height < 200
        }
      ]
    ])