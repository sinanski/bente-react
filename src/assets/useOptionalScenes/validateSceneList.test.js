import {validateSceneList } from "./validateSceneList";
import { createSceneForValidation } from "./createSceneForValidation";
import { validateScene } from "./validateScene";


jest.mock('./createSceneForValidation');
jest.mock('./validateScene');

describe( 'validateSceneList', () => {
  it( 'should return only the first valid scene', () => {
    validateScene
    .mockReturnValueOnce(false)
    .mockReturnValueOnce(true)
    .mockReturnValueOnce(true);

    const mockReturn = [
      {scene: 'mock scene 1'},
      {scene: 'mock scene 2'},
      {scene: 'mock scene 3'},
    ];

    createSceneForValidation
    .mockReturnValueOnce(mockReturn[0])
    .mockReturnValueOnce(mockReturn[1])
    .mockReturnValueOnce(mockReturn[2])

    const x = validateSceneList([1,2, 3], 'mock session');

    expect(validateScene).toHaveBeenCalledWith(mockReturn[0], 'mock session');
    expect(validateScene).toHaveBeenCalledWith(mockReturn[1], 'mock session');
    expect(x).toBe(mockReturn[1].scene);
  } );
} );
