import {validateOptionalScenes} from "./validateOptionalScenes";
import { validateSceneList } from "./validateSceneList";
import { validateScene } from "./validateScene";

jest.mock('./validateSceneList');
jest.mock('./validateScene');

const mockScene = 'mock scene';
const mockScene2 = 'mock scene 2';
const mockScene3 = 'mock scene 3';
const mockSession = 'mock session';

describe( 'validateOptionalScenes', () => {

  beforeEach(jest.clearAllMocks)

  it( 'should call validateScene for single scene', () => {
    validateOptionalScenes([mockScene], mockSession);

    expect(validateScene).toHaveBeenCalledWith(mockScene, mockSession);
    expect(validateSceneList).not.toHaveBeenCalled();
  } );

  it( 'should call validateSceneList for scene lists', () => {
    validateOptionalScenes([[mockScene]], mockSession);

    expect(validateSceneList).toHaveBeenCalledWith([mockScene], mockSession);
    expect(validateScene).not.toHaveBeenCalled();
  } );

  it( 'should return all valid scenes as an array', () => {
    validateScene.mockReturnValue(true);
    validateSceneList.mockReturnValue('Mock return single scene from list');
    const result = validateOptionalScenes(
      [[], mockScene2, mockScene3],
      mockSession
    );

    const expected = [
      'Mock return single scene from list',
      'mock scene 2',
      'mock scene 3'
    ];

    expect(result).toEqual(expected);
  } );
} );
