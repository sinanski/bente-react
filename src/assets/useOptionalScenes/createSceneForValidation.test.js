import { createSceneForValidation } from "./createSceneForValidation";

describe( 'createSceneForValidation should create a scene that offers all props to be validated', () => {
  it( 'should work with optional scenes and add validation that allways validates true', () => {
    const x = createSceneForValidation({scene: 'mock scene'});

    const expected = {
      scene: 'mock scene',
      times_visited: 0,
      validate: expect.any(Function)
    };

    expect(x).toEqual(expected);
  } );

  it( 'should work with optional scenes that offer validation', () => {
    const given = {
      scene: 'mock scene',
      times_visited: 'mock visits',
      validate: jest.fn()
    };

    const x = createSceneForValidation(given);

    expect(x).toEqual(given);
  } );

  it( 'should work with plain scenes ', () => {
    const x = createSceneForValidation({frames: 'mock frames'});

    const expected = {
      scene: {frames: 'mock frames'},
      times_visited: 0,
      validate: expect.any(Function)
    };

    expect(x).toEqual(expected);
  } );
} );
