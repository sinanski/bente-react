import Adapter from 'enzyme-adapter-react-16';
import { configure } from "enzyme";
import { renderHook } from '@testing-library/react-hooks';
import { useOptionalScenes } from "./useOptionalScenes";
import { useSessionForValidation } from "./useSessionForValidation";
import { validateOptionalScenes } from "./validateOptionalScenes";

jest.mock( './useSessionForValidation' );
jest.mock( './validateOptionalScenes' );

configure( { adapter: new Adapter() } );


describe( 'useOptionalScenes', () => {
  it( '', () => {
    useSessionForValidation.mockReturnValue( 'mock session' );
    validateOptionalScenes.mockReturnValue( 'mock return' );
    const { result } = renderHook( () => useOptionalScenes( 'mock scene' ) );

    expect( useSessionForValidation ).toHaveBeenCalled();
    expect( validateOptionalScenes ).toHaveBeenCalledWith( 'mock scene', 'mock session' );
    expect( result.current ).toBe( 'mock return' );
  } )
} );
