import { validateScene } from "./validateScene";
import { createSceneForValidation } from "./createSceneForValidation";
import { hasBeenPlayed } from "./hasBeenPlayed";

jest.mock( './createSceneForValidation' );
jest.mock( './hasBeenPlayed' );

describe( 'validateScene', () => {
  it( 'should call right methods', function () {
    const mockValidator = jest.fn(() => true);
    const mockReturn = {
      id: 'mock return scene',
      times_visited: 1,
      validate: mockValidator
    };

    createSceneForValidation.mockReturnValue( mockReturn );
    validateScene( 'mock scene', 'mock session' );

    expect(createSceneForValidation).toHaveBeenCalledWith('mock scene');
    expect(mockValidator).toHaveBeenCalledWith('mock session');
    expect(hasBeenPlayed).toHaveBeenCalledWith(mockReturn, 'mock session');
  } );

  it( 'should check if scene is valid to be played', () => {
    const givenScene = {
      times_visited: 2,
      validate: () => true
    };
    hasBeenPlayed.mockReturnValue( false );
    createSceneForValidation.mockReturnValue( givenScene );
    const invalid = validateScene( 'mock scene', { times_visited: 1 } );
    const valid = validateScene( null, { times_visited: 2 } );

    expect( invalid ).toBe( false );
    expect( valid ).toBe( true );
  } );

  it( 'should check if scene is valid to ba played', () => {
    const givenScene = {
      times_visited: 0,
      validate: () => false
    };
    createSceneForValidation.mockReturnValue( givenScene );
    const invalid = validateScene( null, { times_visited: 1 } );

    expect( invalid ).toBe( false );
  } );

  it( 'should return false if scene is played / filtered', () => {
    const givenScene = {
      times_visited: 2,
      validate: () => true
    };
    createSceneForValidation.mockReturnValue( givenScene );
    hasBeenPlayed.mockReturnValue(true)
    const invalid = validateScene( null, { times_visited: 1 } );

    expect( invalid ).toBe( false );
  } )
} );
