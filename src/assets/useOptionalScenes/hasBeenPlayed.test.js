import {hasBeenPlayed} from "./hasBeenPlayed";
import {mockValidateableScene, mockSession, setMockSession} from "./mocks";

describe( 'hasBeenPlayed', () => {
  it( 'should return false if scene is neither in played, nor in filtered', () => {
    const has = hasBeenPlayed(mockValidateableScene, mockSession);

    expect(has).toBe(false);
  } );

  it( 'should return true if scene is already played / filtered', () => {
    const sessionPlayed = setMockSession("played", [mockValidateableScene.scene.id]);
    const sessionFiltered = setMockSession("filtered_scenes", [mockValidateableScene.scene.id]);
    const played = hasBeenPlayed(mockValidateableScene, sessionPlayed);
    const filtered = hasBeenPlayed(mockValidateableScene, sessionFiltered);

    expect(played).toBe(true);
    expect(filtered).toBe(true);
  } );
} );
