export const mockValidateableScene = {
  scene: {
    id: 'mock scene',
    frames: []
  },
  times_visited: 'mock visits',
  validate: jest.fn()
};

export { mockSession, setMockSession } from "../../test_utilz/mockStore";
