import { useSessionForValidation } from "./useSessionForValidation";
import { validateOptionalScenes } from "./validateOptionalScenes";
import { OptionalScenes } from "./types";

export const useOptionalScenes = (scenes: OptionalScenes) => {
  const session = useSessionForValidation();

  return validateOptionalScenes(scenes, session);
};
