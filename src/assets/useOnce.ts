import { useState } from "react";

export const useOnce = (): [boolean, () => void] => {
  const [ first, setFirst ] = useState( true );
  const stop = () => setFirst( false );

  return [ first, stop ]
};
