export enum Colors {
  PINK = '#f123c9',
  YELLOW = '#fcba02',
  WHITE = '#fefffe',
  BLACK = '#000000',
  GREY = '#171717'
}
