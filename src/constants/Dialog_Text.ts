export enum Dialog_Text {
  CONTACT_ERROR_NO_MAIL = 'Das sieht für mich nicht nach einer E-Mail Adresse aus, hast du dich vielleicht verschrieben?',
  CONTACT_ERROR_NO_TEXT = "Gibt's auch was, was du mir sagen willst?",
  CONTACT_SEND = "Danke für die Nachricht",
  VISIT_TIMES_2 = "Hey, na. Du schon wieder?",
  VISIT_TIMES_5 = "Sag mal stalkst du mich?",
}

export default Dialog_Text;
