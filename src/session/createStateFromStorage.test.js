import { createStateFromStorage } from "./createStateFromStorage";
import { session } from "./store/actions/methods/session";
import { initialState } from "./store";


jest.mock( './store/actions/methods/session', () => ({
  session: {
    readState: jest.fn()
  }
}) );

const defaultState = {
  ...initialState,
  times_visited: initialState.times_visited + 1,
  is_read: true
};


describe( 'createStateFromStorage', () => {
  it( 'should return initial state if no local state is found', () => {
    session.readState.mockReturnValue();
    const state = createStateFromStorage();

    expect( state ).toEqual( defaultState );
  } );

  it( 'should return initial state if local state is empty', () => {
    session.readState.mockReturnValue( {} );
    const state = createStateFromStorage();

    expect( state ).toEqual( defaultState );
  } );

  it( 'should merge state with initial state and return it', () => {
    session.readState.mockReturnValue( { mock: 'me' } );
    const state = createStateFromStorage();

    const expected = {
      ...defaultState,
      mock: 'me'
    };

    expect( state ).toEqual( expected );
  } );

  it( 'should merge state and update from local storage', () => {
    session.readState.mockReturnValue( { filtered_scenes: [ 'mock_scene' ] } );
    const state = createStateFromStorage();

    const expected = {
      ...defaultState,
      filtered_scenes: [ 'mock_scene' ],
    };

    expect( state ).toEqual( expected );
  } );
} );
