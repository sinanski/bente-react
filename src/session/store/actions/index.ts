import { agreeToDsgvo } from "./methods/agreeToDsgvo";
import { readSession } from "./methods/readSession";
import { clearSession } from "./methods/clearSession";
import { addVisitedPage } from "./methods/addVisitedPage";
import { initialize } from "./methods/initialize";
import { getState } from "./methods/getState";

export default {
  agreeToDsgvo,
  readSession,
  clearSession,
  addVisitedPage,
  initialize,
  getState
}