import { Store } from "use-global-hook";
import {State, Actions} from "../../types";
import { session } from "./session";

export const agreeToDsgvo = (
  store: Store<State, Actions>,
) => {
  store.setState( {
    ...store.state,
    dsgvo_agreed: true
  } );
  session.writeState(store.state)
};