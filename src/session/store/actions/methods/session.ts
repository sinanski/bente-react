import { State } from "../../types";
import { read, write } from "./storePersistantly";

export const session = {
  writeState: (state: State) => write( 'state', state ),
  readState: () => read( 'state' ),
  clear: () => localStorage.clear()
};