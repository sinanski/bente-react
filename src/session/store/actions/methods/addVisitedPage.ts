import { Store } from "use-global-hook";
import {State, Actions} from "../../types";

export const addVisitedPage = (
  store: Store<State, Actions>,
  page: string
) => {
  const {visited_pages} = store.state;
  const set = new Set(page);
  const new_visited = set.has(page)
    ? visited_pages
    : visited_pages.concat(page);

  store.setState( {
    ...store.state,
    visited_pages: new_visited
  } );
};
