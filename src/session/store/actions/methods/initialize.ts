import { Store } from "use-global-hook";
import { Actions, State } from "../../types";
import { session } from "./session";

export const initialize = (store: Store<State, Actions>) => {
  const state = session.readState() || {};
  store.setState({
    ...store.state,
    ...state,
    times_visited: (state.times_visited || 0) + 1,
    is_read: true
  });
  session.writeState(store.state)
};
