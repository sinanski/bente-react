import { Store } from "use-global-hook";
import { State, Actions } from "../../types";

export const getState = (
  store: Store<State, Actions>,
) => {
  const { is_read } = store.state;
  return is_read
    ? store.state
    : null
};
