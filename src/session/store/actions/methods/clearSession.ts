import { Store } from "use-global-hook";
import { Actions, State } from "../../types";
import { initialState } from "../../index";
import { session } from "./session";

export const clearSession = (
  store: Store<State, Actions>
) => {
  session.clear();
  store.setState({
    ...initialState,
    is_read: true
  })
};
