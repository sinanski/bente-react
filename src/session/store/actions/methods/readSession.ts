import { session } from "./session";

export const readSession = () =>
  session.readState();
