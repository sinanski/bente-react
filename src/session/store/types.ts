export type State = {
  version: number;
  dsgvo_agreed: boolean,
  times_visited: number,
  visited_pages: string[],
  filtered_scenes: string[],
  is_read: boolean,
  screen_width: number,
  screen_height: number,
}

export type Actions = {
  isReady(): boolean;
  agreeToDsgvo(): void;
  clearSession(): void;
  addVisitedPage(to: string): void;
  timesVisited(): number;
  dsgvo(): boolean;
  update(state: State, message?: string): void;
  addFinishedSceneToFilters(id: string): void;
  isPortrait(): boolean;
}
