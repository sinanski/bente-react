import gsap from 'gsap/all';
import CustomEase from "./assets/gsap/CustomEase";

gsap.registerPlugin( CustomEase );
