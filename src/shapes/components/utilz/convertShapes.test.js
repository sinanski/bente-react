import { convertShapes } from "./convertShapes";
import { toRender } from "./toRender";

jest.mock( './toRender' );

const mockObject = {
  mock: 'mock value',
  coordinates: [ 1, 2 ]
};

describe( './convertShapes', () => {
  it( 'should ', function () {
    toRender.mockReturnValue( [ mockObject ] );
    const result = convertShapes();
    const expected = [ { ...mockObject, points: '1, 2' } ];

    expect( result ).toEqual( expected )
  } );
} );
