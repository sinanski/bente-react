import { toRender } from "./toRender";

describe('toRender()', () => {
  it( 'should return children if provided', function () {
    const mock = ['mock']
    const children = toRender(mock);

    expect(children).toEqual(mock);
  } );
  it( 'should return an array of state if no children are provided', function () {
    const children = toRender([], 'mock');

    expect(children).toEqual(['mock']);
  } );
});
