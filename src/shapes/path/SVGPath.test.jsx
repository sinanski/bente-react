import React from "react";
import SVGPath from "./SVGPath";
import { shallow } from '../../TestRig/enzyme';

const mockPoints = [
  [ '_mock 1', '_mock 2' ],
  [ '_mock 3', '_mock 4' ]
];

describe( 'SVGPath', () => {
  it( 'should ', function () {
    const Path = shallow(
      <SVGPath
        points={ mockPoints }
      />
    );
    const svgProps = Path.find( 'svg' ).props();
    const pathProps = Path.find( 'svg path' ).props();
    const expectedPathProps = {
      strokeWidth: '25',
      stroke: 'black',
      strokeLinejoin: 'miter',
      fill: 'none',
      d: 'M_mock 1 _mock 2 L_mock 3 _mock 4'
    };
    const expectedSvgStyle = { overflow: 'visible', width: '100%' };

    expect( svgProps.style ).toEqual( expectedSvgStyle );
    expect( pathProps ).toEqual( expectedPathProps );
  } );
} );
