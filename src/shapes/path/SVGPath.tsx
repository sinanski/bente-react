import React from "react";

interface Props {
  points: number[][];
  pathRef (ref: any): void
}

const createPath = (points: number[][]) => {
  return points.map(([x, y], i) => {
    const prefix = i === 0 ? 'M' : 'L';
    return `${prefix}${x} ${y}`
  })
  .join(' ')
};

const SVGPath: React.FC<Props> = (
  {
    points,
    pathRef
  }
) => {

  const d = createPath(points);

  return (
    <svg style={{overflow: "visible", width: '100%'}}>
      <path
        ref={pathRef}
        strokeWidth="25"
        stroke="black"
        strokeLinejoin="miter"
        fill="none"
        d={d}>
      </path>
    </svg>
  )
};

export default SVGPath;
