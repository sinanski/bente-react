import {getTextPosition} from "./textPosition";

describe('getTextPosition()', () => {
  it( 'should return nothing if it receives nothing', function () {
    const nothing = getTextPosition();

    expect(nothing).not.toBeDefined();
  } );

  it( 'should return text position if !switchDirection', function () {
    const original = getTextPosition('mock position');

    expect(original).toBe('mock position');
  } );

  it( 'should revert position if switchDirection', function () {
    const given = {
      left: 'mock left',
      right: 'mock right',
      rest: 'rest props',
    };
    const expected = {
      left: 'mock right',
      right: 'mock left',
      rest: 'rest props',
    };

    const given2 = {
      ...given,
      right: undefined
    };

    const expected2 = {
      ...expected,
      left: 'auto'
    };

    const reverted = getTextPosition(given, true);
    const reverted2 = getTextPosition(given2, true);

    expect(reverted).toEqual(expected);
    expect(reverted2).toEqual(expected2);
  } );
});
