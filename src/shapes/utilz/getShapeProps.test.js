import { getShapeProps } from "./getShapeProps";
import { chat_answer, chat_bubble, arrow, rectangle } from "../templates/shapes";
import { chat_answer_mock, chat_bubble_mock, arrow_mock } from "./__mocks__/textPosition";


jest.mock( './textPosition' );
jest.mock( '../templates/shapes' );

describe( '', () => {

  it( 'chat_answer should return right values', function () {
    const expected = {
      form: 'mock answer form',
      text_position: chat_answer_mock,
      text_color: 'black'
    }
    chat_answer.mockReturnValue( expected.form );
    const props = getShapeProps( 'chat_answer' );

    expect( props ).toEqual( expected )
  } );

  it( 'chat_bubble should return right values', function () {
    const expected = {
      form: 'mock bubble form',
      text_position: chat_bubble_mock,
      text_color: 'black'
    }
    chat_bubble.mockReturnValue( expected.form );
    const props = getShapeProps( 'chat_bubble' );

    expect( props ).toEqual( expected )
  } );

  it( 'arrow should return right values', function () {
    const expected = {
      form: 'mock arrow form',
      text_position: arrow_mock,
      text_color: 'white'
    };
    arrow.mockReturnValue( expected.form );
    const props = getShapeProps( 'arrow' );

    expect( props ).toEqual( expected )
  } );

  it( 'rectangle should return default values', function () {
    const expected = {
      form: 'mock rectangle form',
      text_position: {},
      text_color: 'white'
    };
    rectangle.mockReturnValue( expected.form );
    const props = getShapeProps( 'rectangle' );

    expect( props ).toEqual( expected )
  } );
} );
