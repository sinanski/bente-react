import {effects} from "./effects";

describe('Shape onHover', () => {
  it( 'should wiggleOnHover', function () {
    const wiggle = effects('wiggle').wiggle();
    const dont_wiggle = effects(['reverse']).wiggle();

    expect(wiggle).toBe(true);
    expect(dont_wiggle).toBe(false);
  } );

  it( 'should reverseOnHover', function () {
    const reverse = effects(['reverse']).reverse();
    const dont_reverse = effects('wiggle').reverse();

    expect(reverse).toBe(true);
    expect(dont_reverse).toBe(false);
  } );

  it( 'should show on hover', function () {
    const show = effects(['show', 'wiggle']).show();
    const dont_show = effects(['reverse', 'wiggle']).show();

    expect(show).toBe(true);
    expect(dont_show).toBe(false);
  } );
});
