export const chat_answer_mock = 'mock answer text position';
export const chat_bubble_mock = 'mock bubble text position';
export const arrow_mock = 'mock arrow text position';

export default {
  chat_answer: chat_answer_mock,
  chat_bubble: chat_bubble_mock,
  arrow: arrow_mock
}
