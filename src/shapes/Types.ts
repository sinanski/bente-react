export type Coordinates = number[][];

export type Position = {
  x: number;
  y: number;
}

export type PositionOptions = {
  x?: number;
  y?: number;
  extend?: boolean;
}

type StrokeLinejoin = "miter" | "round" | "bevel" | "inherit";

export interface PolygonOptions {
  fill?: string;
  strokeLinejoin?: StrokeLinejoin;
  fillOpacity?: number;
  stroke?: string;
  strokeWidth?: number;
  coordinates?: Coordinates;
  displacementMap?: Coordinates;
}

export interface RectangleOptions extends PolygonOptions {
  width?: number;
  height?: number;
}

export interface CoordsAndDisplace {
  coordinates: Coordinates;
  displacementMap: Coordinates;
}

export interface PolygonState extends PolygonBase, CoordsAndDisplace {
}

export interface PolygonDom extends PolygonBase {
  points: string;
  wiggle?: boolean;
  wiggleOptions?: WiggleOptionProps;
}

export interface PolygonBase {
  fill: string;
  strokeLinejoin: StrokeLinejoin;
  fillOpacity: number;
  stroke: string;
  strokeWidth: number;
}

export type TemplateTypes = 'arrow' | 'chatAnswer' | 'chatBubble' | 'speechBubble' | 'apostrophe';

export interface SVG {
  width: number | string;
  height: number | string;
  preserveAspectRatio: string;
  viewBox: string;
}

export interface ShapeForm extends SVG {
  state: PolygonState;
  children: Array<PolygonState>;
}

export type OnMousePossibilities = 'wiggle' | 'reverse' | 'show';
export type OnMouseOver = OnMousePossibilities | OnMousePossibilities[];

export interface WiggleOptions {
  x: number,
  y: number,
  extend: boolean;
}

export type WiggleOptionProps = Partial<WiggleOptions>;

export interface TemplateInterface extends TemplateProps {
  type: TemplateType;
}

export type ShapeType = 'rectangle' | 'chat_bubble' | 'chat_answer' | 'arrow';

export type TemplateType = ShapeType;

type TextColor = 'black' | 'white';

type StylePosition = number | 'auto';

export type TextPosition = {
  top?: StylePosition;
  bottom?: StylePosition;
  left?: StylePosition;
  right?: StylePosition;
  width?: number;
  height?: number;
}

export interface TemplateProps {
  wiggle?: boolean;
  onMouseOver?: OnMouseOver;
  wiggleOptions?: WiggleOptionProps;
  switchDirection?: boolean;
  containerWidth?: string;
}

export interface ShapeProps extends TemplateProps, CreateShapeProps {
  switchDirection?: boolean;
  containerWidth?: string;
}

export interface CreateShapeProps {
  form: ShapeForm;
  textColor?: TextColor;
  textPosition?: TextPosition;
}

export interface ReturnShapeProps {
  form: ShapeForm;
  text_color: TextColor;
  text_position: TextPosition;
}


export type Partial<T> = {
  [P in keyof T]?: T[P];
};
