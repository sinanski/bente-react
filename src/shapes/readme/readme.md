# Shapes
    import { Template, Shape, ShapeConstructor, Path } from '.../shapes';
    
You can either use a template or create custom shapes.

## Template
Templates can be used to easily set up predefined shapes.

    import { Template } from '.../shapes';
    
    <Template type="speech_bubble" />
    
#### types
* rectangle
* speech_bubble
* chat_bubble
* chat_answer
* arrow_left
* arrow_right
* apostrophe

## Custom Shapes
All types can be used as basics for customization. The most basic usage is

    import { Shape, ShapeConstructor } from '.../shapes';

    const shape = new ShapeConstructor()
        .arrow_left({ fill: 'red' })
    
    <Shape form={ arrow } />
    
![Image of Speech Bubble](./arrow.png)

All types from template are available here, too.
    

## Complex Shapes

Shapes can be modified with different methods. All return the shape object for easy chaining.

    const speech_bubble = new ShapeConstructor()
        .speech_bubble({ fillOpacity: .7 })
        .add()
    
        .grow({ 
            x: 10, 
            y: 10, 
            fill: 'none',
            stroke: 'white',
            strokeWidth: 8
        })
        .randomize({ x: 4, y: 4 })
        .add()
    
        .grow({ x: 5, y: 5, fill: 'black', fillOpacity: 1 }
        .add()
        
    const arrow = new ShapeConstructor()
        .arrow_left({ fill: 'white })
        .add()
        
        .grow({ x: 5, y: 5, fill: 'black' }
        .randomize({ x: 5, y: 5 })
        .add()
        
    const apostrophe = new ShapeConstructor()
        .apostrophe({ fill: 'white })
        .add()
        
        .grow({ x: 5, y: 5, fill: 'black' }
        .randomize({ x: 5, y: 5 })
        .add()
    
    <Shape form={ speech_bubble } />
    <Shape form={ arrow } />
    <Shape form={ apostrophe } />
    
If more than one layer is provided the initial layer needs also to be added with `.add()`
Each newly added layer is positioned in the back of the last one.


![Image of Speech Bubble](./speech-bubble.png)

## SwitchDirection
The switchDirection prop mirrors the shape on a vertical axes.

## Position Text
Shapes accept a position prop that takes in optional key/value pairs for `top`, `bottom`, `left`, `right`, `width`, `height`

##wiggle
The wiggle prop is optional. If set to `true` all points within the shape will wiggle by the default value.

    <Shape form={arrow} wiggle={true} />
    
wiggle can also receive an object containing an optional `x` and `y`. And an optional value for `extend` that is true by default. Extend means that the random points in the wiggle always stretch the shape.

    const wiggleOptions = {
        x: 20, 
        y: 10, 
        extend: false
    }
    
    <Shape form={ arrow } wiggle={ wiggleOptions } />
    
Shapes offer specials for onMouseOver
this prop can either take a vlue of `wiggle`, or `reverse`. Or it can take an array of values which currently only support `['wiggle', 'reverse']`. Of course this means that both actions are triggered onMouseOver

#### Reverse
The reverse works best for two shape because it switches the color values of both shapes. If more shapes are offered, each Shape gats the color of the shape in front while the last shape gets the value of the first.

**a prop is needed for something like**
    
    <Shape onMouseOver=['wiggle', 'reverse'] />
    
## Modifying The underlying SVG

The ShapeConstructor excepts an object of SVG config as an optional argument

    const config = {
        width="100%",
        height="100%",
        viewBox="0 0 1040 350",
        preserveAspectRatio="none",
    }
    
    const shape = new ShapeConstructor( config )

All values are passed to the SVG. All shapes have default values. The shown example uses the default values except for `viewBox` which is provided by the chosen shape object.
  