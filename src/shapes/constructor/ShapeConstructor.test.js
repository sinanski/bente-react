import {ShapeConstructor} from "./ShapeConstructor";

const mockState = {
  "coordinates": [],
    "displacementMap": [],
    "fill": "black",
    "fillOpacity": 1,
    "stroke": "",
    "strokeLinejoin": "miter",
    "strokeWidth": 0,
};

const defaultProps = {
  width: '100%',
  height: '100%',
  fill: 'black',
  preserveAspectRatio: 'none',
  viewBox: '0 0 100 100',
  children: [],
  state: mockState
};

describe('new ShapeConstructor()', () => {
  it( 'should return default Value', function () {
    const shape = new ShapeConstructor();

    expect(shape).toEqual(defaultProps)
  } );

  it( 'should return provided values', function () {
    const expected = {
      width: 'mock width',
      height: 'mock height%',
      fill: 'mock fill',
      preserveAspectRatio: 'mock preserveAspectRatio',
      viewBox: 'mock viewBox',
      children: 'mock children'
    };
    const shape = new ShapeConstructor(expected)

    expect(shape).toEqual(shape)
  } );

  it( 'should only update provided values', function () {
    const expected = {
      ...defaultProps,
      width: 'mock width',
    };
    const shape = new ShapeConstructor(expected);

    expect(shape).toEqual(shape)
  } );

  it( 'should return provided values', function () {
    const shape = new ShapeConstructor();
    shape
    .add('mock 1')
    .add('mock 2');

    // expect(shape.children).toHaveLength(2);
    // expect(shape.children).toEqual(['mock 1', 'mock 2']);
  } );
});
