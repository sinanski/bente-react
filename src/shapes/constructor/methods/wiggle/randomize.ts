import Random from "../randomize/Random";

export function randomize(value: number, amount: number): number {
  const rand = new Random();

  return value
    + rand.limited( amount )
    * rand.coin();
}
