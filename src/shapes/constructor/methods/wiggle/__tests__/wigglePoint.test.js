import { wigglePoint } from "../wigglePoint";
import { randomize } from "../randomize";
import { mockCoordsAndDisplace } from "../../mocks";

jest.mock( '../randomize' );

describe( 'wigglePoint()', () => {
  it( 'should call randomize() with the right values', function () {
    wigglePoint(mockCoordsAndDisplace, {x:10, y: 2});

    expect(randomize).toHaveBeenCalledTimes(6);
    expect( randomize.mock.calls[0] ).toEqual( [ 1, 900 ] );
    expect( randomize.mock.calls[1] ).toEqual( [ 9, 200 ] );
    expect( randomize.mock.calls[2] ).toEqual( [ 2, 800 ] );
    expect( randomize.mock.calls[3] ).toEqual( [ 8, 400 ] );
    expect( randomize.mock.calls[4] ).toEqual( [ 3, 700 ] );
    expect( randomize.mock.calls[5] ).toEqual( [ 7, 600 ] );
  } );
} );
