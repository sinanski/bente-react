import {randomize} from "../randomize";

const mockCoin = jest.fn( () => 10 );
const mockLimited = jest.fn( val => val );

jest.mock('../../randomize/Random', () => {
  return jest.fn().mockImplementation(() => ({
    coin: mockCoin,
    limited: mockLimited,
  }))
});

describe('randomize()', () => {
  it( 'should call Random.coin()', function () {
    randomize();

    expect(mockCoin).toHaveBeenCalled();
  } );

  it( 'should call Random.limited()', function () {
    randomize(1,2);

    expect(mockLimited).toHaveBeenCalledWith(2)
  } );

  it( 'should return the right value', function () {
    const result = randomize(1,2);

    expect(result).toBe(21)
  } );
});
