import { animateWiggle } from "../animateWiggle";
import { wigglePoint } from "../wigglePoint";

jest.mock( '../wigglePoint' );

const mockPoints = 'mock points';
const mockValue = 'mock value';

describe( 'wiggle()', () => {
  it( 'should call wigglePoints()', function () {
    animateWiggle( {}, mockPoints, mockValue );

    expect( wigglePoint ).toHaveBeenCalledTimes( 3 );
    expect( wigglePoint ).toHaveBeenCalledWith( mockPoints, mockValue );
  } );
} );
