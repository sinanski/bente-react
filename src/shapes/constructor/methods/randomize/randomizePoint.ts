import Random from "./Random";

export function randomizePoint(coordinate: number, displacement: number, amount: number) {
  const rand = new Random();

  return coordinate
    + rand.limited( amount )
    * displacement;
}
