export default class Random {
  coin() {
    return Math.random() > .5 ? 1 : -1
  }
  limited(amount: number) {
    return Math.round(Math.random() * (amount - 1) + 1);
  }
}
