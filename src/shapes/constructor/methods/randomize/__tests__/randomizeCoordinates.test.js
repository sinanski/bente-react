import { randomizeCoordinates } from "../randomizeCoordinates";
import { randomizePoint } from "../randomizePoint";
import {mockCoordinates, mockDisplacement, mockX, mockY} from '../../mocks';

jest.mock( '../randomizePoint' );

describe( 'randomizeCoordinates()', () => {
  it( 'should call randomizePoint() correctly', function () {
    randomizeCoordinates( mockCoordinates, mockDisplacement, mockX, mockY );

    expect( randomizePoint ).toHaveBeenCalledTimes( 6 );
    expect( randomizePoint.mock.calls[0] ).toEqual( [ 1, 90, mockX ] );
    expect( randomizePoint.mock.calls[1] ).toEqual( [ 9, 100, mockY ] );
    expect( randomizePoint.mock.calls[4] ).toEqual( [ 3, 70, mockX ] );
    expect( randomizePoint.mock.calls[5] ).toEqual( [ 7, 300, mockY ] );
  } );

  it( 'should return an array of arrays of results from randomizePoint', function () {
    randomizePoint.mockReturnValue('mock return');
    const result = randomizeCoordinates( mockCoordinates, mockDisplacement );

    expect(result).toHaveLength(3);
    expect(result[0]).toHaveLength(2);
    expect(result[0]).toContain('mock return');
  } );
} );
