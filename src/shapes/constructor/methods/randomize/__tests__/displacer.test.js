import {displacer} from "../displacer";
import { calculateDisplacement } from "../calculateDisplacement";

jest.mock('../calculateDisplacement');

const mockDisplacementMap = 'mock map'

describe('displacer()', () => {

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it( 'should return given displacementMap if extends = true', function () {
    const result = displacer(mockDisplacementMap, true);

    expect(calculateDisplacement).not.toHaveBeenCalled();
    expect(result).toBe(mockDisplacementMap);
  } );

  it( 'should return given displacementMap as default', function () {
    const result = displacer(mockDisplacementMap);

    expect(calculateDisplacement).not.toHaveBeenCalled();
    expect(result).toBe(mockDisplacementMap);
  } );

  it( 'should call calculateDisplacement() if extends != true', function () {
    calculateDisplacement.mockReturnValue('mock displaced');
    const result = displacer(mockDisplacementMap, false);

    expect(calculateDisplacement).toHaveBeenCalledWith(mockDisplacementMap);
    expect(result).toBe('mock displaced');
  } );
});
