import { calculateDisplacement } from "../calculateDisplacement";
import Random from "../Random";

jest.mock( '../Random' );

const mockDisplacementMap = [
  [ 1, 10 ],
  [ 2, 100 ],
  [ 3, 1000 ],
];

describe( 'calculateDisplacement()', () => {

  beforeEach( () => {
    jest.clearAllMocks();
  } );

  it( 'should call Random.coin() for each value in number[][]', function () {
    calculateDisplacement( mockDisplacementMap );
    const randomInstance = Random.mock.instances[0];

    expect( Random ).toHaveBeenCalled();
    expect( randomInstance.coin ).toHaveBeenCalledTimes( 6 );
  } );

  it( 'should return displacedMap', function () {
    const mockRandom = 'mock value'
    const coin = jest.fn( () => mockRandom );
    Random.mockImplementation( () => ({ coin }) );
    const displacedMap = calculateDisplacement( mockDisplacementMap );

    expect( displacedMap ).toHaveLength( 3 );
    expect( displacedMap[0] ).toHaveLength( 2 );
    expect( displacedMap[0] ).toContain( mockRandom );
  } );
} );
