import {randomizePoint} from "../randomizePoint";
import Random from "../Random";

jest.mock('../Random');

const mockCoordinate = 1;
const mockDisplacement = 10;
const mockAmount = 100;

describe('randomizePoint()', () => {
  it( 'should call Random()', function () {
    const limited = jest.fn( () => mockAmount );
    Random.mockImplementation( () => ({ limited }) );
    const result = randomizePoint(mockCoordinate, mockDisplacement, mockAmount);
    const expectedResult = mockDisplacement * mockAmount + mockCoordinate;

    expect(limited).toHaveBeenCalledWith(mockAmount);
    expect(result).toBe(expectedResult);
  } );
});
