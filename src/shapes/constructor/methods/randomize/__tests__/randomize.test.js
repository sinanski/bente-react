import {randomize} from "../randomize";
import {displacer} from "../displacer";
import {randomizeCoordinates} from "../randomizeCoordinates";

jest.mock('../displacer');
jest.mock('../randomizeCoordinates');

const mockOptions = {
  x: 'mock x',
  y: 'mock y',
  extend: 'mock extend'
};

const that = {
  coordinates: 'mock coordinates',
  displacementMap: 'mock displacementMap',
};

const mockDisplacerReturn = 'mock displacer return'

describe('randomize()', () => {

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it( 'should call displacer()', function () {
    randomize(mockOptions, that);

    expect(displacer).toHaveBeenCalledWith('mock displacementMap', 'mock extend')
  } );

  it( 'should return the right value', function () {
    displacer.mockReturnValue(mockDisplacerReturn);
    randomize(mockOptions, that);

    expect(randomizeCoordinates).toHaveBeenCalledWith(
      "mock coordinates",
      mockDisplacerReturn,
      "mock x",
      "mock y"
    )
  } );
});
