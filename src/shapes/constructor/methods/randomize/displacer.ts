import { Coordinates } from "../../../Types";
import { calculateDisplacement } from "./calculateDisplacement";

export const displacer = (displacementMap: Coordinates, extend = true) =>
  extend
    ? displacementMap
    : calculateDisplacement( displacementMap );
