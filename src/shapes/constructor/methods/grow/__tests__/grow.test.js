import { grow } from "../grow";
import { growShape } from "../growShape";

jest.mock('../growShape');

const mockX = 'mock x';
const mockY = 'mock y';
const mockCoordinates = 'mock Coordinates';
const mockDisplacementMap = 'mock DisplacementMap';
const mockReturn = 'mock return';

const mockOptions = {
  x: mockX,
  y: mockY,
};

const mockThat = {
  coordinates: 'mock Coordinates',
  displacementMap: 'mock DisplacementMap'
};

describe( 'grow()', () => {
  it( 'should call growShape()', function () {
    grow( mockOptions, mockThat );

    expect( growShape ).toHaveBeenCalledWith(
      mockX,
      mockY,
      mockCoordinates,
      mockDisplacementMap
    )
  } );

  it( 'should return the value of growShape()', function () {
    growShape.mockReturnValue(mockReturn);
    const result = grow( mockOptions, mockThat );

    expect(result).toBe(mockReturn)
  } );
} );
