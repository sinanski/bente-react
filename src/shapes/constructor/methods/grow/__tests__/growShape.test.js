import {growShape} from "../growShape";

const x = 1;
const y = 2;

const mockCoordinates = [
  [ 1, 7 ],
  [ 2, 8 ],
  [ 3, 9 ],
];

const mockDisplacementMap = [
  [ 10, 100 ],
  [ 20, 200 ],
  [ 30, 300 ],
];

describe( 'growShape()', () => {
  it( 'should return grown coordinates', function () {
    const grown = growShape( x, y, mockCoordinates, mockDisplacementMap );
    const expected = [ [ 11, 207 ], [ 22, 408 ], [ 33, 609 ] ];

    expect( grown ).toEqual( expected )
  } );
} );