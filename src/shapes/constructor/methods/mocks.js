export const mockCoordinates = [
  [ 1, 9 ],
  [ 2, 8 ],
  [ 3, 7 ],
];

export const mockDisplacement = [
  [ 90, 100 ],
  [ 80, 200 ],
  [ 70, 300 ],
];

export const mockX = 'mock x';
export const mockY = 'mock y';

export const mockCoordsAndDisplace = {
  coordinates: mockCoordinates,
  displacementMap: mockDisplacement
};
