import TimelineMax from 'assets/utilz/timeline';


export const animateLeft = (quotes: any) => ({
  in: () => animateLeftIn( quotes.ref ).play().delay(4),
});

const animateLeftIn = (quotes: HTMLDivElement) => new TimelineMax( { paused: true } )
  .to( quotes.childNodes, {
    duration: 1,
    x: 0,
    stagger: .6,
    ease: 'power4.out'
  } );