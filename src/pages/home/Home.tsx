import React from 'react';
import Cover from "./Cover";
import Quotes from "./Quotes";
import ReadMoreButton from "./ReadMore";

const Home = () => {

  return (
    <>
      <Cover />
      <Quotes />
    </>
  );
};

export default Home
