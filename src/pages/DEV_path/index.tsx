import React, { useState } from 'react';
import styled from 'styled-components';
import TestUi from "../components/TestUi";

const Container = styled.div`
  display: flex;
  width: 500px;
  height: 100%;
`;

// The ChatPath receives boolean[] with the question is this an answer
// This implementation is not working anymore just as showcase

const Index = () => {
  const initialState: boolean[] = [];
  const [ points, setPoints ] = useState( initialState );

  const buttons = [
    {
      text: 'Add Point',
      onClick: () => setPoints( points.concat( false ) )
    },
    {
      text: 'Add Answer',
      onClick: () => setPoints( points.concat( true ) )
    }
  ];

  return (
    <Container>
      Hier gibt es nichts zu sehen. Bitte gehen sie weiter!
      <TestUi buttons={ buttons } />
    </Container>
  );
};

export default Index
